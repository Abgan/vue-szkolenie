import { Security } from './Security';
import { SearchResponse, Album, Artist, Track } from '../model/Album';

export class Search {
    constructor(private security: Security) {}

    public searchSpotify(query: string, type: string): Promise<Album[]|Artist[]|Track[]> {
        return fetch(`https://api.spotify.com/v1/search?q=${query}&type=${type}&market=PL`,
            { headers: { Authorization: 'Bearer ' + this.security.getToken() },
             },
            ).then<Response>( (resp) => {
                if (resp.status === 401) {
                    this.security.authorize();
                }
                return resp;
            })
            .then<SearchResponse>((resp) => resp.json()).then( (resp) => {
                if (resp.hasOwnProperty('artists')) {
                    return resp.artists!.items;
                } else if (resp.hasOwnProperty('albums')) {
                    return resp.albums!.items;
                } else if (resp.hasOwnProperty('tracks')) {
                    return resp.tracks!.items;
                }
                return [];
                });
    }
}
