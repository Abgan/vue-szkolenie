export class Security {
    private token = '';

    constructor(
        readonly url = 'https://accounts.spotify.com/authorize',
        readonly clientId = '6106e162fd7e4eb5a9d49b73bffd7052',
        readonly redirectUri = 'http://localhost:8080/',
        readonly responseType = 'token',
    ) {}

    public authorize() {
        const url = `${this.url}?client_id=${this.clientId}\
&response_type=${this.responseType}&redirect_uri=${this.redirectUri}`;
        localStorage.removeItem('token');
        location.replace(url);
    }

    public getToken() {
        this.token = JSON.parse(localStorage.getItem('token') || 'null');
        if (!this.token) {
            const match = location.hash.match(/access_token=([^&]+)/);
            this.token = match ? match[1] : '';
            localStorage.setItem('token',
                JSON.stringify(this.token));
            location.hash = '';
        }
        if (!this.token) {
            this.authorize();
        }
        return this.token;
    }
}
