import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import PlaylistsView from './views/PlaylistsView.vue';
import MusicSearch from './views/MusicSearch.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/Playlists',
      name: 'Playlists',
      component: PlaylistsView,
    },
    {
      path: '/Search',
      name: 'Search',
      component: MusicSearch,
    },
  ],
});
