
interface Entity {
    id: string;
    name: string;
    type: string;
}

export interface Album extends Entity {
    artists?: Artist[];
    images?: AlbumImage[];
    label?: string;
    release_date?: string;
}

export interface Artist extends Entity {
    popularity?: number;
    images?: AlbumImage[];
    uri?: string;
    followers?: {
        total: number;
    };
}

export interface Track extends Entity {
    popularity?: number;
    track_number: number;
    album?: {
        name: string;
    };
    artists?: [{
        name: string;
    }];
    explicit: boolean;
    duration_ms: number;
    uri: string;
}

export interface AlbumImage {
    url: string;
    width?: number;
    height?: number;
}

export interface PagingObject<T> {
    items: T[];
    total: number;
}

export interface SearchResponse {
    albums?: PagingObject<Album>;
    artists?: PagingObject<Artist>;
    tracks?: PagingObject<Track>;
}

export type Results = Album[] | Artist[] | Track[];
