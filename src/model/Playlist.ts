export interface Playlist {
    id: number;
    name: string;
    fav: boolean;
    /**
     * Hex color
     */
    color: string;
    tracks?: Track[];
}

export interface Track {
    id: number;
    title: string;
}
