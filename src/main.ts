import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import { security } from './services';
import Card from '@/components/cards/Card.vue';

security.getToken();
Vue.config.productionTip = false;
Vue.filter('shorten', (value: string, max = 20) =>
 value.length >= max ? value.substring(0, max) + '...' : value);
Vue.component('Card', Card);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');

