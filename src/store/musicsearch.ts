import { Module } from 'vuex';
import { Playlist } from '@/model/Playlist';
import { Results } from '@/model/Album';
import { search } from '@/services';



interface State {
    results: Results;
    type: 'album' | 'artist' | 'track';
    query: string | null;
}

const MusicStore: Module<State, {}> = {
    state: {
        results: [],
        type: 'album',
        query: null,
    },
    mutations: {
        setResults(state, {results, type}) {
            state.results = results;
            state.type = type;
        },
        setType(state, type) {
            state.type = type;
        },
        setQuery(state, query) {
            state.query = query;
        },
    },
    actions: {
        search({state, commit}, query: string) {
            commit('setQuery', query);
            search.searchSpotify(query, state.type).then( (results: any) => {
                let rtype = 'album';
                if (results[0]) {
                    rtype = results[0].type;
                }
                commit('setResults', {
                    type: rtype,
                    results,
                });
            });
        },
        setType({state, commit}, newType: string) {
            commit('setType', newType);
        },
        setQuery({commit}, newQuery: string) {
            commit('setQuery', newQuery);
        },
    },
    getters: {
        query(state) {
            return state.query;
        },
        results(state) {
            return state.results;
        },
        resultType(state) {
            let rtype = 'album';
            if (state.results[0]) {
                rtype = state.results[0].type;
            }
            return rtype;
        },
    },
};

export default MusicStore;
