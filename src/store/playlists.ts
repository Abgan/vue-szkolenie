import { Module } from 'vuex';
import { Playlist } from '@/model/Playlist';

interface State {
    list: Playlist[];
    selectedId: Playlist['id'] | null;
}

const PlayStore: Module<State, {}> = {
    state: {
        list: [
            {
            id: 123,
            name: 'Wishmaster',
            fav: true,
            color: '#4328d7',
          }, {
            id: 124,
            name: 'Sleeping Sun',
            fav: true,
            color: '#b5732b',
          }, {
            id: 125,
            name: 'Wish I had an angel',
            fav: false,
            color: '#e6f10e',
          },
        ],
        selectedId: null,
    },
    mutations: {
        selectPlaylist(state, payload) {
            state.selectedId = payload;
        },
        updatePlaylist(state, payload) {
            const index = state.list.findIndex( (p) => p.id === payload.id);
            state.list.splice(index, 1, payload);
        },
        addPlaylist(state, payload) {
            state.list.push(payload);
        },
        removePlaylist(state, payload) {
            const index = state.list.findIndex( (p) => p.id === payload);
            if (index !== -1) {
                state.list.splice(index, 1);
            }
        },
    },
    actions: {
        selectPlaylist({state, commit}, id: Playlist['id']) {
            commit('selectPlaylist', state.selectedId === id ? null : id);
        },
        removePlaylist({state, commit}, id) {
            if (id === state.selectedId) {
                commit('selectPlaylist', id);
            }
            commit('removePlaylist', id);
        },
        savePlaylist({state, commit}, payload) {
            const index = state.list.findIndex( (p) => p.id === payload.id);
            if (index !== -1) {
                commit('updatePlaylist', payload);
            } else {
                commit('addPlaylist', payload);
            }
            commit('selectPlaylist', payload.id);
        },
    },
    getters: {
        playlists(state) {
            return state.list;
        },
        selectedPlaylist(state) {
            return state.list.find( (p) => p.id === state.selectedId);
        },
    },
};

export default PlayStore;
