import Vue from 'vue';
import Vuex from 'vuex';
import PlayStore from './store/playlists';
import MusicStore from './store/musicsearch';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    PlayStore,
    MusicStore,
  },
  state: {},
  mutations: {},
  getters: {},
  actions: {},
});
