import { Security } from './services/Security';
import { Search } from './services/search';
import { SearchResponse, Album } from './model/Album';

export const security = new Security();
export const search = new Search(security);

